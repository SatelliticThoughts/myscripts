echo "Set up directories"

mkdir ~/Applications
mkdir ~/Projects
mkdir ~/Scripts

echo
echo "Update System"

sudo eopkg up -y


echo
echo "Install development files"

sudo eopkg it -c system.devel


echo
echo "Install Applications"

sudo eopkg it -y blender gimp krita lmms geany vim thunderbird discord audacity glade tilix dmd dub docker


echo
echo "Set up vim"

mkdir ~/Scripts/vim
git clone https://gitlab.com/SatelliticThoughts/vim.git ~/Scripts/vim
sudo pip3 install GitPython
cd ~/Scripts/vim; python3 ~/Scripts/vim/setup.py


# echo
# echo "Set up flatpak and install additional applications"

# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpak.repo
# sudo flatpak install flathub com.google.AndroidStudio
# sudo flatpak install flathub com.jetbrains.IntelliJ-IDEA-Community
# sudo flatpak install flathub org.keepassxc.KeePassXC


echo
echo "Set up docker"

sudo systemctl enable docker
sudo systemctl start docker
sudo groupadd docker
sudo usermod -aG docker $USER


echo
echo "Set up finished"

